@NonCPS
def getCommentsString() {
    def list = []
    def changeLogSets = currentBuild.changeSets               
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            if (entry.msg != null) {
               list.add("""${entry.commitId} on ${new Date(entry.timestamp)}: ${entry.msg.replaceAll("[\n\r]", " ")}""")
            } 
        } 
    } 
    return list.join(',') 
}
  
def SendAutoTestsFailed(){
      def commits = getCommentsString() 
      def payload = """
                         [                           
                            {
                                "fields": [ 
                                    {
                                        "key": "Param1", 
                                        "value": "Automatic tests has failed for Build ${JOB_NAME}, version ${env.VERSION}.${env.BUILD_ID} "
                                    },
                                    {
                                        "key": "Param2", 
                                        "value": "Automatic Tests has failed for Jenkins build: <a href='https://jenkinsdemo.syncnow.io/job/${JOB_NAME}/${env.BUILD_ID}'>#${env.BUILD_ID}</a>"
                                    },
                                    {
                                        "key": "Param3",
                                        "value": "<a href='https://jenkinsdemo.syncnow.io/job/${JOB_NAME}/${env.BUILD_ID}'>${JOB_NAME}: Build #${env.BUILD_ID}</a>"
                                    },                                    
                                    {
                                        "key": "Param4",
                                        "value": "${env.VERSION}.${env.BUILD_ID}"
                                    }
                                ], 
                                "entityUniqueKey": null,
                                "eventRelativeURL": "/job/${JOB_NAME}/${env.BUILD_ID}",
                                "entityType": "Bug",
                                "subProject": "CLS",
                                "comments": "${commits}",
                                "commentLinkOrRelationTitle": "Automatic tests has failed for Build '${JOB_NAME}', version ${env.VERSION}.${env.BUILD_ID}"
                            } 
                        ]  
                     """ 
                
                     string responseCreateUpdate = httpRequest acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'POST', authentication: 'SyncNow', requestBody: "${payload}", url: "https://syncnowdemo.syncnow.io/api/v1.0/app/DevOpsGate/Enrich/127?action=create&outputAsStringWithHashedKeys=true"                                        
                     
                    
                     println("Create Entity Status:  ${responseCreateUpdate.status}")
                     println("Create Entity Content: ${responseCreateUpdate.content}")
                                            
                     sh """curl 'Content-Type: application/x-www-form-urlencoded' -F 'attachments=@/${env.WORKSPACE}/log.txt' -F 'comments=${responseCreateUpdate.content}'  https://syncnowdemo.syncnow.io/api/v1.0/app/DevOpsGate/Enrich/127/Attachment"""
          
                     
            
    return payload;
}

def PublishBuildInformation(){
      def commits = getCommentsString() 
      def payload = """
                         [                         
                            {                               
                                "eventRelativeURL": "/job/${JOB_NAME}/${env.BUILD_ID}",                                                             
                                "comments": "${commits}",
                                "commentLinkOrRelationTitle": "Item Published in Build '${JOB_NAME}', version ${env.VERSION}.${env.BUILD_ID}"
                            } 
                        ]  
                     """ 
                         
                     string responseComment = httpRequest acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'POST', authentication: 'SyncNow', requestBody: "${payload}", url: "https://syncnowdemo.syncnow.io/api/v1.0/app/DevOpsGate/Enrich/129?action=comment"                                        
                      
                     println("Comment Entity Status:  ${responseComment.status}")
                     println("Comment Entity Content: ${responseComment.content}")     
                     
                     
                     def payloadVer = """
                         [                         
                            {
                                "fields": [ 
                                    {
                                        "key": "Param1", 
                                        "value": "${env.VERSION}.${env.BUILD_ID} "
                                    },
                                    {
                                        "key": "Param2", 
                                        "value": "Vulnerable Component Found"
                                    }    
                                ],                                                          
                               "entityType": "Story",
                                "comments": "${commits}"                                
                            } 
                        ]  
                     """
                     
                    println("Payload:  ${payloadVer}")

                    // Publish build version 
                     
                    string responseVer = httpRequest acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'POST', authentication: 'SyncNow', requestBody: "${payloadVer}", url: "https://syncnowdemo.syncnow.io/api/v1.0/app/DevOpsGate/Enrich/129?action=update"                                        
                      
                    println("Build Info Status:  ${responseVer.status}")
                    println("Build Info Content: ${responseVer.content}")       
                                     
   
}

 
pipeline { 
    agent any
     
    stages {
        stage('Clone') {
            steps {
                echo 'Clone Code'
            }
        }
        stage('Version') {
            steps { 
                echo 'Version'
            }
        }
      
        stage("SonarQube Analysis") {            
            steps {
                   script{
                         def commits = getCommentsString() 
                         def scannerHome = tool 'main_sonar_scanner';
                         withSonarQubeEnv('sonarmain') {
                               sh """${scannerHome}/bin/sonar-scanner -Dsonar.analysis.param.Param1='Sonar Quality Tests for Build ${JOB_NAME}' -Dsonar.analysis.param.Param4='${env.VERSION}.${env.BUILD_ID}' -Dsonar.analysis.targetDetail.Project=CLS -Dsonar.analysis.targetDetail.CommitComments='${commits}' -Dsonar.analysis.targetDetail.EventRelativeURL='/job/${JOB_NAME}/${env.BUILD_ID}' -Dsonar.analysis.targetDetail.CommentLinkOrRelationTitle='SonarQube Report' -Dsonar.analysis.targetDetail.EntityType='Story' """
                        }
                   } 
            } 
        }
      
      
      
        stage('Automatic Tests') {
            steps {               
                  script{
                    echo "Test ${params['Fail Tests']}"
                    if("${params['Fail Tests']}"=="true"){
                        echo 'Opening a bug - Failed Tests'
                        SendAutoTestsFailed();
                    }
                }
            }
        }
        
        stage('Build') {
            steps {
                echo 'Build'
            }
        } 
        
        stage('Publish') {
            steps {
                echo 'Publish'                
            }
        }
        
        stage('Publish To SyncNow') {
            steps {                                            
                script {
                      echo "Publish To SyncNow"
                      echo "Job Name is ${JOB_NAME}, Build ID is ${env.BUILD_ID}"
                      
                      PublishBuildInformation();
                                             
                     //Link Build to all releated issues
                     //string responseLink = httpRequest acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'POST', authentication: 'SyncNow', requestBody: "${payload}", url: "https://syncnowdemo.syncnow.io/api/v1.0/app/DevOpsGate/LinkIt/1429"                                        
                      
                     //println("Link Entity Status:  ${responseLink.status}")
                     //println("Link Entity Content: ${responseLink.content}")
                     
                     //Add comment with the build URL to all releated issues
                    // string responseComment = httpRequest acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'POST', authentication: 'SyncNow', requestBody: "${payload}", url: "https://syncnowdemo.syncnow.io/api/v1.0/app/DevOpsGate/Enrich/1429?action=comment"                                        
                      
                     //println("Comment Entity Status:  ${responseComment.status}")
                     //println("Comment Entity Content: ${responseComment.content}")
                } 
            }
        }
    }
}